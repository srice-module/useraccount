<?php

namespace Modules\UserAccount\Console;

use Illuminate\Console\Command;
use Nwidart\Modules\Facades\Module;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class InstallUserAccount extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'install:useraccount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(setting('module.user.account.active') == 0) {
            $module = Module::find('UserAccount');
            $module->enable();
            $this->call("migrate");
            setting(["module.user.account.active" => 1])->save();
            $this->info("Le module 'User Account' est activé");
        }else{
            $this->error("Le module est déja installer");
        }
        return null;
    }
}
